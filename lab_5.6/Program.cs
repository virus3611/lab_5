﻿using System;

namespace lab_5._6
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Введите строку: ");
            String line = Console.ReadLine();

            int word = 0;    //слово
            int number = 0;    // колличество слов
            for (int i = 0; i < line.Length; i++)
            {
                    if (line[i] != ' ')
                    {
                        if (i + 1 != line.Length)
                            number += 1;
                        else
                            word += 1;
                    }
                    else if(number > 0)
                    {
                        word += 1;
                        number = 0;
                    }
            }
            Console.WriteLine("Кол-во слов: {0}", word);
        }
    }
}