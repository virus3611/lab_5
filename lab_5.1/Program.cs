﻿using System;

namespace lab_5
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int[] array = GenerateArray();
            ArrayPrint(array);
            Console.WriteLine("\nКоличество чисел в массиве, кратных 3-ём: {0}", TheNumberMultipleOfThree(array));
            Console.WriteLine("Сумма положительных элементов массива {0}", SumOfPositiveNumbers(array));
            Console.WriteLine("Номер максимального положительного числа: {0}", MaximumPositiveNumber(array));
            PrintZeroValue(array);
            PrintPositiveValue(array);            
        }

        public static int[] GenerateArray() //Заполняем массив рандомными числами
        {
            Random random = new Random();
            int[] array = new int[15];
            for (int i = 0; i < 15; i++)
                array[i] = random.Next(-50, 50);
            return array;
        }

        public static int TheNumberMultipleOfThree(int[] array) //количество чисел, кратных трем
        {
            int counter = 0;
            foreach (int i in array)
            {
                if (i % 3 == 0)
                    counter++;
            }

            return counter;
        }

        public static int SumOfPositiveNumbers(int[] array)    // сумма положительных элементов массива
        {
            int sum = 0;
            foreach (int i in array)
            {
                if (i > 0)
                    sum += i;
            }

            return sum;
        }

        public static int MaximumPositiveNumber(int[] array)    // максиммальное положительное число
        {
            int maxValue = 0, index = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > maxValue)
                {
                    maxValue = array[i];
                    index = i;
                }
            }
            
            return index;
        }

        public static void PrintZeroValue(int[] array)    // есть ли в массиве нулевые элементы
        {
            int nullValuse = 0, index = 0;
            for(int i = 0; i < array.Length; i++)
            {
                if (array[i] == 0)
                {
                    nullValuse = array[i];
                    index = i;
                    Console.WriteLine("Array[{0}] = {1}", index, nullValuse);
                }
            }
        }

        public static void PrintPositiveValue(int[] array)    // вывод элементов с четными номерами
        {
            int value = 0;
            for(int i = 0; i < array.Length; i++)
            {
                if(i % 2 == 0)
                {
                    value = array[i];
                    Console.WriteLine("array[{0}] = {1}", i, value);
                }
            }
        }
        public static void ArrayPrint(int[] array)
        {
            foreach (int i in array) Console.Write(" " + i + "; ");
        }
    }
}