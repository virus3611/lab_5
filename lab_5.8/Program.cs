﻿using System;

namespace lab_5._8
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int[][,] jagger = new int[3][,];
            jagger[0] = new int[2,2];
            jagger[1] = new int[3,2];
            jagger[2] = new int[2,3];
            
            FillArray(ref jagger);
            GetMatrix(jagger);
        }

        static void FillArray(ref int[][,] jagger)
        {
            Random random = new Random();
            for (int dimension = 0; dimension < jagger.Length; dimension++)
            {
                for (int i = 0; i < jagger[dimension].GetLength(0); i++)
                    for (int j = 0; j < jagger[dimension].GetLength(1); j++)
                        jagger[dimension][i, j] = random.Next(0, 255);
            }
        }
        
        static void GetMatrix(int[][,] matrix)
        {
            int sumMax = 0;

            for (int dimension = 0; dimension < matrix.Length; dimension++)
            {
                for (int i = 0; i < matrix[dimension].GetLength(0); i++)
                {
                    int max = Int32.MinValue;
                    for (int j = 0; j < matrix[dimension].GetLength(1); j++)
                    {
                        if (max < matrix[dimension][i, j])
                            max = matrix[dimension][i, j];
                    }
                    
                    Console.WriteLine("Максимум {1} столбца {2} матрицы = {0}", max, i + 1, dimension + 1);
                    sumMax += max;
                }
            }

            Console.WriteLine("Сумма макс. = {0}", sumMax);
        }
    }
}