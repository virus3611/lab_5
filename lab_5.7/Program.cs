﻿using System;
using System.Text;

namespace lab_5._7
{
    internal class Program
    {
        static void Main(string[] args) 
        { 
            Console.Write("Введите длину строки:"); 
            int l = Convert.ToInt32(Console.ReadLine()); 
            StringBuilder mainString = new StringBuilder(l) { Length = l }; 
            const string baseChars = "jklmnopqr012345?!;"; 
            Random random = new Random(); 
            for (int i=0;i<mainString.Length;i++) 
            { 
                mainString[i]=baseChars[random.Next(0,baseChars.Length)]; 
            } 
            Console.WriteLine(mainString.ToString()); 
            for (int i = 0; i < mainString.Length; i++) 
            { 
                if (mainString[i] == '!') 
                    mainString[i] = '_'; 
            } 
            Console.WriteLine(mainString.ToString()); 
        }
    }
}