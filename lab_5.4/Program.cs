﻿using System;

namespace lab_5._4
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Введите кол-во столбцов: ");
            int m = Int32.Parse(Console.ReadLine());
            Console.Write("Введите кол-во строк: ");
            int n = Int32.Parse(Console.ReadLine());
            
            int[,] matrix = GenerateArray(m, n);
            
            EchoArray(matrix);
            
            MinInRowArray(matrix);
            
            Console.WriteLine("Ср. ариф: {0}", Average(matrix));
        }
        
        private static int[,] GenerateArray(int m, int n)
        {
            Random random = new Random(DateTime.Now.Millisecond);
            int[,] array = new int[m, n];

            for (int col = 0; col < m; col++)
            {
                for (int row = 0; row < n; row++)
                    array[col, row] = random.Next(-100, 100);
            }
            
            return array;
        }
        
        private static void EchoArray(int[,] array)
        {
            for (int col = 0; col < array.GetLength(0); col++)
            {
                for (int row = 0; row < array.GetLength(1); row++)
                    Console.Write("{0, 4}", array[col, row]);
                Console.WriteLine();
            }
        }

        private static void MinInRowArray(int[,] array)
        {
            for (int i = 0; i < array.GetLength(0); i++)
            {
                int min = array[i, 0];
                for (int j = 0; j < i; j++)
                {
                    if (array[i, j] < min)
                        min = array[i, j];
                }
                
                Console.WriteLine("Минимальное значение {0} строки: {1}", i + 1, min);
            }
        }
        
        private static int MinArray(int[,] array)
        {
            int min = Int32.MaxValue;
            foreach (var item in array)
                if (min > item)
                    min = item;

            return min;
        }
        
        private static int MaxArray(int[,] array)
        {
            int max = Int32.MinValue;
            foreach (var item in array)
                if (max < item)
                    max = item;

            return max;
        }

        private static int Average(int[,] array)
        {
            int min = MinArray(array);
            int max = MaxArray(array);

            int amount = 0;
            foreach (var item in array)
                if (item != min && item != max)
                    amount += item;
           
            
            return amount / (array.Length - 2);
        }
    }
}