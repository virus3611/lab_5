﻿using System;
using System.Linq;

namespace lab_5._2
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int[] array = GenerateArray();
         
            Console.Write("Исходный массив: ");
            EchoArray(array);
            
            array = ShiftArray(array);

            Console.Write("Новый массив: ");
            EchoArray(array);
        }
        
        private static void EchoArray(int[] array)
        {
            foreach (var item in array)
                Console.Write($"{item} ");
            Console.WriteLine();
        }
        private static int[] GenerateArray()
        {
            Random random = new Random();
            int[] array = new int[15];
 
            for (int i = 0; i < 15; i++)
                array[i] = random.Next(-100, 100);

            return array;
        }

        private static int[] ShiftArray(int[] array)
        {
            int first = array[0];
            
            for (int i = 1; i < array.Length; i++)
                array[i - 1] = array[i];

            array[array.Length - 1] = first;

            return array;
        }
    }
}