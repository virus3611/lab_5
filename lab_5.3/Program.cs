﻿using System;

namespace lab_5._3
{
    internal class Program
    {
        public static void Main(string[] args)
        {
                int[] outputArray = new int[9];
                Console.Write("Enter the size of the array: ");
                int size = int.Parse(Console.ReadLine());
                int[] stringArray = new int[size];
                Console.WriteLine("Enter string of numbers: ");
                for(int i = 0; i < stringArray.Length; i++)
                {
                    stringArray[i] = int.Parse(Console.ReadLine());
                }

                foreach (int j  in stringArray)
                {
                    outputArray[j-1]++;
//                    outputArray[stringArray[j - 1]]++;
                }
                Console.WriteLine(string.Join(" ",stringArray));
                Console.WriteLine(string.Join(" ", outputArray));
        }
    }
}