﻿using System;

namespace lab_5._5
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Введите кол-во столбцов: ");
            int m = Int32.Parse(Console.ReadLine());
            Console.Write("Введите кол-во строк: ");
            int n = Int32.Parse(Console.ReadLine());
 
            int[,] matrix = GenerateArray(m, n);
 
            EchoArray(matrix);
 
            Console.WriteLine("Колличество локальных минимумов: {0}", CountLocalMinArray(matrix, m, n));
        }
 
        private static int[,] GenerateArray(int m, int n)
        {
            Random random = new Random(DateTime.Now.Millisecond);
            int[,] array = new int[m + 2, n + 2];

            for (int i = 0; i < array.GetLength(0); i++) 
            { 
                for (int j = 0; j < array.GetLength(1); j++) 
                { 
                    array[i, j] = Int32.MaxValue; 
                } 
            } 
 
            for (int col = 1; col < array.GetLength(0) - 1; col++)
            {
                for (int row = 1; row < array.GetLength(0) - 1; row++)
                    array[col, row] = random.Next(0, 100);
            }

            return array;
        }
 
        private static void EchoArray(int[,] array)
        {
            for (int col = 1; col < array.GetLength(0) - 1; col++)
            {
                for (int row = 1; row < array.GetLength(1) - 1; row++)
                    Console.Write("{0, 4}", array[col, row]);
                Console.WriteLine();
            } 
        }
 
        private static int CountLocalMinArray(int[,] array, int m, int n)
        {
            int сount = 0;
            for (int i = 1; i < array.GetLength(0) - 1; i++)
            {
                for (int j = 1; j < array.GetLength(1) - 1; j++)
                {
                    if (array[i, j] < array[i + 1, j] &&
                        array[i, j] < array[i - 1, j] &&
                        array[i, j] < array[i, j + 1] &&
                        array[i, j] < array[i, j - 1])
                    {
                        сount++;
                    }
                }
            }

            return сount;
        }
    }
}